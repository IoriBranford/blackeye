local levity = require "levity"
local CollisionRules = require "CollisionRules"
require "class"

local FixedCam = class(function(self, object)
	self.object = object
	self.object.body:setFixedRotation(true)
	self.object.body:setGravityScale(0)

	local cx, cy = self.object.body:getWorldCenter()
	levity.camera:set(cx, cy, self.object.width, self.object.height)

	self:refreshFixture()
end)

function FixedCam:refreshFixture()
	for _, fixture in ipairs(self.object.body:getFixtureList()) do
		fixture:destroy()
	end

	local fixture = love.physics.newFixture(self.object.body,
					 love.physics.newRectangleShape(
					 levity.camera.w/2, levity.camera.h/2,
					 levity.camera.w, levity.camera.h))
	fixture:setCategory(CollisionRules.Category_Camera)
	fixture:setMask(CollisionRules.Category_Objects,
			CollisionRules.Category_HiddenObjects)
end

return FixedCam
