require "class"
local levity = require "levity"
local PlayerShot = levity:requireScript("PlayerShot")
local function initPlayer(self, object)
	object.body:setFixedRotation(true)
	self.object = object
	self.dpad = {x = 0, y = 0}
end

local Player = class(initPlayer)

function Player:beginContact(otheruserdata, contact)
	-- TODO only if from enemy bullet
	self.object.dead = true
	levity:playSound("explode_m.wav")
end

local keyforce = 240

function Player:keypressed(key, u)
	if key == "up" then
		self.dpad.y = self.dpad.y - 1
	elseif key == "down" then
		self.dpad.y = self.dpad.y + 1
	elseif key == "left" then
		self.dpad.x = self.dpad.x - 1
	elseif key == "right" then
		self.dpad.x = self.dpad.x + 1
	elseif key == "m" then
		levity:playSound("mechani8.s3m")
	elseif key == "z" then
		local shot = PlayerShot.create(
			self.object.x + 32, self.object.y, 960, 0,
			self.object.layer)
		levity:playSound("player_shot.wav")
	end
end

function Player:keyreleased(key, u)
	if key == "up" then
		self.dpad.y = self.dpad.y + 1
	elseif key == "down" then
		self.dpad.y = self.dpad.y - 1
	elseif key == "left" then
		self.dpad.x = self.dpad.x + 1
	elseif key == "right" then
		self.dpad.x = self.dpad.x - 1
	elseif key == "f9" then
		levity:setNextMap("maps/stealth_test.lua")
	end
end

function Player:beginMove(dt)
	local body = self.object.body
	local vx0, vy0 = body:getLinearVelocity()
	local vx1, vy1 = keyforce * self.dpad.x, keyforce * self.dpad.y
	local mass = body:getMass()
	body:applyLinearImpulse(mass * (vx1-vx0), mass * (vy1-vy0))
	levity.camera:set(body:getX(), body:getY())
end

return Player
