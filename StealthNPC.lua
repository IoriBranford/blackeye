local levity = require "levity"
require "class"
local CollisionRules = require "CollisionRules"

local Bullet = require "Bullet"

local StealthNPC = class(function(self, id)
	self.object = levity.map.objects[id]
	self.object.body:setFixedRotation(true)
	for _, fixture in ipairs(self.object.body:getFixtureList()) do
		fixture:setCategory(CollisionRules.Category_Objects)
	end
	self.dpad = {x = 0, y = 0}
	local flipx, flipy = levity:getGidFlip(self.object.gid)
	if flipx then
		self.dpad.x = -1
		self:faceDirection(-1)
	else
		self.dpad.x = 1
		self:faceDirection(1)
	end
	self.stair = 0
	self.groundobject = nil
	self:targetEnemy(nil)
end)

StealthNPC.WalkSpeed = 300
StealthNPC.RunSpeed = 480
StealthNPC.SightRange = 180

function StealthNPC:beginContact_body(yourfixture, otherfixture, contact)
	-- properties of dynamic object's fixture or of static object

	local fixturedata = otherfixture:getUserData()
	local fixtureproperties = fixturedata.properties
	local fixtureobject = fixturedata.object

	local normx, normy = contact:getNormal()

	if not otherfixture:isSensor() then
		if math.abs(normy) > math.abs(normx) then
			self.groundobject = fixtureobject
		elseif math.abs(normy) < math.abs(normx) then
			self.dpad.x = -self.dpad.x
		end
	end

	-- properties of dynamic object itself

	local bodydata = otherfixture:getBody():getUserData()
	if not bodydata then
		return
	end
	local bodyproperties = bodydata.properties
	local bodyobject = bodydata.object

	--if bodyobject.id == self.object.properties.targetenemyid then
	--	self:targetEnemy(nil)
	--end
end

function StealthNPC:endContact_body(yourfixture, otherfixture, contact)
	-- properties of dynamic object's fixture or of static object

	local otherdata = otherfixture:getUserData()
	local otherproperties = otherdata.properties
	local otherobject = otherdata.object

	--local normx, normy = contact:getNormal()
	--if math.abs(normy) > math.abs(normx) then
		if otherobject == self.groundobject then
			self.groundobject = nil
		end
	--end
end

function StealthNPC:beginContact(yourfixture, otherfixture, contact)
	local yourfixturename = yourfixture:getUserData().object.name
	local bc = self["beginContact_"..yourfixturename]
	if bc then
		bc(self, yourfixture, otherfixture, contact)
	end
end

function StealthNPC:endContact(yourfixture, otherfixture, contact)
	local yourfixturename = yourfixture:getUserData().object.name
	local ec = self["endContact_"..yourfixturename]
	if ec then
		ec(self, yourfixture, otherfixture, contact)
	end
end

function StealthNPC:faceDirection(dir)
	self.facedirection = dir
	levity:setObjectGid(self.object,
		     levity:setGidFlip(self.object.gid, dir < 0, false))
	self:targetEnemy(self.object.properties.targetenemyid)
end

function StealthNPC:targetEnemy(targetid)
	local fixtures = self.object.body:getFixtureList()
	if targetid then
		for _, fixture in ipairs(fixtures) do
			fixture:setMask()
			fixture:setRestitution(0.25)
		end
	else
		for _, fixture in ipairs(fixtures) do
			fixture:setMask(CollisionRules.Category_HiddenObjects)
			fixture:setRestitution(0.25)
		end
	end
	self.object.properties.targetenemyid = targetid
end

function StealthNPC:seeObjects()
	local eyesx, eyesy = cmx, cmy
	local eyesfixture = self.object.body:getUserData().fixtures["eyes"]
	if eyesfixture then
		eyesx, eyesy = eyesfixture:getShape():getPoint()
		eyesx, eyesy = self.object.body:getWorldPoint(eyesx, eyesy)
	end

	local nearestfixture = nil
	local nearestfrac = math.huge
	levity.world:rayCast(eyesx, eyesy,
		eyesx + StealthNPC.SightRange * self.facedirection, eyesy,
		function(fixture, x, y, normx, normy, frac)
			if fixture:isSensor() then
				return -1
			end

			if frac < nearestfrac then
				nearestfixture = fixture
				nearestfrac = frac
			end
			return 1
		end)

	if nearestfixture then
		local objectdata = nearestfixture:getBody():getUserData()

		if objectdata then
			if objectdata.properties.hidden == "false" then
				local name = objectdata.object.name
				if name and name == "player" then
					self:targetEnemy(objectdata.object.id)
				end
			end
		end
	end
end

function StealthNPC:beginMove(dt)
	local body = self.object.body
	local cmx, cmy = body:getWorldCenter()
	local vx0, vy0 = body:getLinearVelocity()

	self:seeObjects()

	local speed = StealthNPC.WalkSpeed
	local targetenemy
	if self.object.properties.targetenemyid then
		targetenemy =
			levity.map.objects[self.object.properties.targetenemyid]
	end
	if targetenemy then
		local tcmx, tcmy = targetenemy.x, targetenemy.y
		if targetenemy.body then
			tcmx, tcmy = targetenemy.body:getWorldCenter()
		end
		if self.dpad.x * (tcmx - cmx) < 0 then
			self.dpad.x = tcmx - cmx
			self.dpad.x = self.dpad.x / math.abs(self.dpad.x)
		end
		speed = StealthNPC.RunSpeed
	else
		self:targetEnemy(nil)

		local groundobject = self.groundobject
		if groundobject then
			local gx1, gx2 = groundobject.x,
				groundobject.x +
				(groundobject.w or groundobject.width)

			local nextcmx = cmx + vx0*dt
			if nextcmx < gx1 or gx2 < nextcmx then
				self.dpad.x = -self.dpad.x
			end
		end
	end

	if self.dpad.x ~= 0 then
		self:faceDirection(self.dpad.x)
	end

	local vx1 = 0
	if math.abs(vy0) < 1 then
		vx1 = speed * self.dpad.x
	end
	--local vy1 = self.stair * vx1

	local mass = body:getMass()
	body:applyLinearImpulse(mass * (vx1 - vx0), 0)--mass * (vy1 - vy0))
end

function StealthNPC:beginDraw()
	if self.object.properties.targetenemyid then
		love.graphics.setColor(255, 0, 0)
	else
		love.graphics.setColor(0, 0, 255)
	end
end

function StealthNPC:endDraw()
	love.graphics.setColor(255, 255, 255)
end

return StealthNPC
