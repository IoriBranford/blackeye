local levity = require "levity"
local CollisionRules = require "CollisionRules"
require "class"

local FollowCam = class(function(self, id)
	self.object = levity.map.objects[id]
	self.object.body:setFixedRotation(true)
	self.object.body:setGravityScale(0)

	local cx, cy = self.object.body:getWorldCenter()
	levity.camera:set(cx, cy, self.object.width, self.object.height)

	self:refreshFixture()
end)

function FollowCam:refreshFixture()
	for _, fixture in ipairs(self.object.body:getFixtureList()) do
		fixture:destroy()
	end

	local fixture = love.physics.newFixture(self.object.body,
					 love.physics.newRectangleShape(
					 levity.camera.w/2, levity.camera.h/2,
					 levity.camera.w, levity.camera.h))
	fixture:setCategory(CollisionRules.Category_Camera)
	fixture:setMask(CollisionRules.Category_Objects,
			CollisionRules.Category_HiddenObjects)
end

function FollowCam:beginMove(dt)
	local body = self.object.body
	local vx0, vy0 = body:getLinearVelocity()
	local vx1, vy1 = 0, 0

	local leaderid = tonumber(self.object.properties.leaderid)
	local leader = levity.map.objects[leaderid]
	if leader and leader.body then
		vx1, vy1 = leader.body:getLinearVelocity()
	end

	local mass = 4 * dt * body:getMass()
	body:applyLinearImpulse(mass * (vx1-vx0), mass * (vy1-vy0))
end

function FollowCam:endMove(dt)
	local cx, cy = self.object.body:getWorldCenter()
	levity.camera:set(cx, cy)
end

return FollowCam
