local levity = require "levity"
require "class"
local CollisionRules = require "CollisionRules"
local Bullet = levity.machine:requireScript("Bullet")

local StealthPlayer = class(function(self, id)
	self.object = levity.map.objects[id]
	self.object.body:setFixedRotation(true)
	for _, fixture in ipairs(self.object.body:getFixtureList()) do
		fixture:setCategory(CollisionRules.Category_Objects)
	end
	self.dpad = {x = 0, y = 0}
	self.object.properties.hidden = "false"
	local flipx, flipy = levity:getGidFlip(self.object.gid)
	if flipx then
		self:faceDirection(-1)
	else
		self:faceDirection(1)
	end

	-- downright/upleft = 1, downleft/upright = -1
	self.stair = 0
end)

StealthPlayer.WalkSpeed = 240
StealthPlayer.HiddenOpacity = 64

function StealthPlayer:beginContact_body(yourfixture, otherfixture, contact)
	-- properties of dynamic object's fixture or of static object

	local fixturedata = otherfixture:getUserData()
	local fixtureproperties = fixturedata.properties

	if fixtureproperties.doortype == "hide" then
		self.object.properties.hidden = "true"
		yourfixture:setCategory(CollisionRules.Category_HiddenObjects)
		return
	end

	if fixtureproperties.stair then
		local polyline = fixturedata.object.polygon
		local dy = (polyline[2].y - polyline[1].y)
		local dx = (polyline[2].x - polyline[1].x)
		if dx ~= 0 then
			self.stair = dy / dx
		else
			self.stair = 0
		end
		return
	end

	if fixtureproperties.doortype == "exit" then
		levity.bank:play("gmwin.ogg")
		self.object.dead = true
		return
	end

	-- properties of dynamic object itself

	local bodydata = otherfixture:getBody():getUserData()
	if not bodydata then
		return
	end
	local bodyproperties = bodydata.properties

	if bodyproperties.targetenemyid == self.object.id then
		levity.bank:play("gmlose.ogg")
		self.object.dead = true
		return
	end
end

function StealthPlayer:endContact_body(yourfixture, otherfixture, contact)
	local otherdata = otherfixture:getUserData()
	local otherproperties = otherdata.properties

	if otherproperties.doortype == "hide" then
		self.object.properties.hidden = "false"
		yourfixture:setCategory(CollisionRules.Category_Objects)
	end

	if otherproperties.stair then
		self.stair = 0
	end
end

function StealthPlayer:beginContact(yourfixture, otherfixture, contact)
	local yourfixturename = yourfixture:getUserData().object.name
	local bc = self["beginContact_"..yourfixturename]
	if bc then
		bc(self, yourfixture, otherfixture, contact)
	end
end

function StealthPlayer:endContact(yourfixture, otherfixture, contact)
	local yourfixturename = yourfixture:getUserData().object.name
	local ec = self["endContact_"..yourfixturename]
	if ec then
		ec(self, yourfixture, otherfixture, contact)
	end
end

function StealthPlayer:faceDirection(dir)
	self.facedirection = dir
	levity:setObjectGid(self.object,
		     levity:setGidFlip(self.object.gid, dir < 0, false))
end

function StealthPlayer:keypressed_left()
	self.dpad.x = self.dpad.x - 1
	self:faceDirection(-1)
end

function StealthPlayer:keypressed_right()
	self.dpad.x = self.dpad.x + 1
	self:faceDirection(1)
end

--function StealthPlayer:keypressed_z()
--	local gunfixture = self.object.body:getUserData().fixtures["gun"]
--	local gunx, guny = gunfixture:getShape():getPoint()
--	gunx, guny = self.object.body:getWorldPoint(gunx, guny)
--	Bullet.create(gunx, guny,
--			self.facedirection * 10240, 0, self.object.layer)
--	levity.bank:play("pistol-fire.wav")
--end

function StealthPlayer:keyreleased_left()
	self.dpad.x = self.dpad.x + 1
end

function StealthPlayer:keyreleased_right()
	self.dpad.x = self.dpad.x - 1
end

function StealthPlayer:touchpressed(touch, x, y)
	self:touchmoved(touch, x, y, x - (love.graphics.getWidth() / 2), 0)
end

function StealthPlayer:touchmoved(touch, x, y, dx, dy)
	local leftkey = love.graphics.getWidth() / 4
	local rightkey = leftkey * 3

	if x < leftkey then
		if x - dx >= leftkey then
			self:keypressed_left()
		end
	else
		if x - dx < leftkey then
			self:keyreleased_left()
		end
	end

	if x >= rightkey then
		if x - dx < rightkey then
			self:keypressed_right()
		end
	else
		if x - dx >= rightkey then
			self:keyreleased_right()
		end
	end
end

function StealthPlayer:touchreleased(touch, x, y)
	self:touchmoved(touch,
            love.graphics.getWidth() / 2, 0,
			(love.graphics.getWidth() / 2) - x, 0)
end

function StealthPlayer:beginMove(dt)
	local body = self.object.body
	local vx0, vy0 = body:getLinearVelocity()

	local vx1 = 0
	if math.abs(vy0) < 1 then
		vx1 = StealthPlayer.WalkSpeed * self.dpad.x
		vx1 = math.max(-StealthPlayer.WalkSpeed,
				math.min(vx1, StealthPlayer.WalkSpeed))
	end
	--local vy1 = self.stair * vx1

	local mass = body:getMass()
	body:applyLinearImpulse(mass * (vx1 - vx0), 0)--mass * (vy1 - vy0))
end

function StealthPlayer:beginDraw()
	if self.object.properties.hidden == "true" then
		love.graphics.setColor(255, 255, 255,
			StealthPlayer.HiddenOpacity)
	end
end

function StealthPlayer:endDraw()
	love.graphics.setColor(255, 255, 255)
end

return StealthPlayer
