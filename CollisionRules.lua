local bit = require "bit"

local CollisionRules = {
	Category_Objects = 1,
	Category_Camera = 2,
	Category_LevelBounds = 3,
	Category_HiddenObjects = 4
}
--[[
local function mask(inverted, ...)
	local mask = 0
	if inverted then
		local mask = bit.tobit(-1)
	end

	local categories = {...}
	for _, category in ipairs(categories) do
		if inverted then
			mask = bit.band(mask, bit.bnot(bit.lshift(1, category - 1)))
		else
			mask = bit.bor(mask, bit.lshift(1, category - 1))
		end
	end
	return mask
end

function CollisionRules.onlyCollideMask(...)
	return mask(true, ...)
end

function CollisionRules.dontCollideMask(...)
	return mask(false, ...)
end
]]
return CollisionRules
