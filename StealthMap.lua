local levity = require "levity"
local CollisionRules = require "CollisionRules"
require "class"

local StealthMap = class(function(self, id)
	self.map = levity.map
	for _, fixture in ipairs(self.map.box2d_collision.body:getFixtureList()) do
		fixture:setCategory(CollisionRules.Category_Objects)
	end

	local width = self.map.width * self.map.tilewidth
	local height = self.map.height * self.map.tileheight

	local boundsshapes = {
		love.physics.newEdgeShape(0, 0, 0, height),
		love.physics.newEdgeShape(0, 0, width, 0),
		love.physics.newEdgeShape(width, 0, width, height),
		love.physics.newEdgeShape(0, height, width, height)
	}

	for _, shape in ipairs(boundsshapes) do
		local fixture = love.physics.newFixture(self.map.box2d_collision.body, shape)
		fixture:setCategory(CollisionRules.Category_LevelBounds)
	end

	self.musicsource = levity.bank:play("gmtactic.ogg")
	self.shader = love.graphics.newShader([[
#ifdef GL_ES 
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif
		extern number radius;
		extern vec2 center;
		vec4 effect(vec4 color, Image texture, vec2 tc, vec2 sc)
		{
			number d = length(center - sc) / radius;
			number v = smoothstep(0.0, 1.0, 1.0 - d);
			return Texel(texture, tc) * color * v;
		}
	]])
end)

StealthMap.DarknessRadius = 480

function StealthMap:endMove()
	local player = self.map.objects[tonumber(self.map.properties.playerid)]
	if player then
	elseif self.musicsource then
		love.audio.stop(self.musicsource)
		self.musicsource = nil
	end
end

function StealthMap:beginDraw()
	local player = self.map.objects[tonumber(self.map.properties.playerid)]
	if player and player.body then
		local x, y = player.body:getWorldCenter()
		local intscale = math.floor(levity.camera.scale)
		x = x - levity.camera.x
		y = y - levity.camera.y
		self.shader:send("center", { x * intscale, y * intscale })
		self.shader:send("radius", self.DarknessRadius * intscale)
	end
	love.graphics.setShader(self.shader)
end

function StealthMap:endDraw()
	--love.graphics.setShader()
	--love.graphics.setStencilTest()
end

return StealthMap
