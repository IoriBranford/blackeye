local levity = require "levity"
require "class"

local PlayerShot = class(function(self, object)
	object.body:setBullet(true)
	self.object = object
end)

function PlayerShot.create(x, y, vx, vy, layer)
	local shot = {
		x = x,
		y = y,
		shape = "ellipse",
		width = 4,
		height = 4,
		properties = {
			script = "PlayerShot"
		}
	}
	levity:addObject(shot, layer, "dynamic")
	shot.body:applyLinearImpulse(shot.body:getMass()*vx, shot.body:getMass()*vy)
	return shot
end

function PlayerShot:beginContact(otheruserdata, contact)
	-- hit spark
	-- destroy self
	self.object.dead = true
end

return PlayerShot
