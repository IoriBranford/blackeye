local levity = require "levity"
local CollisionRules = require "CollisionRules"
require "class"

local Bullet = class(function(self, id)
	self.object = levity.map.objects[id]
	self.object.body:setBullet(true)
	for _, fixture in ipairs(self.object.body:getFixtureList()) do
		fixture:setCategory(CollisionRules.Category_Objects)
	end
end)

function Bullet:beginContact(yourfixture, otherfixture, contact)
	if not otherfixture:isSensor() then
		-- hit spark
		-- destroy self
		self.object.dead = true
	end
end

function Bullet.create(x, y, vx, vy, layer)
	local shot = {
		x = x,
		y = y,
		shape = "ellipse",
		width = 4,
		height = 4,
		properties = {
			script = "Bullet",
			collidable = "true"
		}
	}
	levity:addObject(shot, layer, "dynamic")
	local mass = shot.body:getMass()
	shot.body:applyLinearImpulse(mass * vx, mass * vy)
	return shot
end

return Bullet
