<?xml version="1.0" encoding="UTF-8"?>
<tileset name="kanna" tilewidth="64" tileheight="64" tilecount="1" columns="1">
 <tileoffset x="-32" y="32"/>
 <image source="../images/kanna.png" width="64" height="64"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="0" x="16" y="16" width="32" height="32"/>
  </objectgroup>
 </tile>
</tileset>
