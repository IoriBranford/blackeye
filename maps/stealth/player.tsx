<?xml version="1.0" encoding="UTF-8"?>
<tileset name="player" tilewidth="128" tileheight="256" tilecount="1" columns="1">
 <image source="player.png" width="128" height="256"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="0" name="body" x="32" y="64" width="64" height="192"/>
   <object id="0" name="eyes" x="100" y="80">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
   <object id="0" name="feet" x="64" y="256">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
   <object id="0" name="gun" x="112" y="128">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
</tileset>
