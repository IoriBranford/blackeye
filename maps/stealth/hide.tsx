<?xml version="1.0" encoding="UTF-8"?>
<tileset name="hide" tilewidth="128" tileheight="256" tilecount="1" columns="1">
 <image source="hide.png" width="128" height="256"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="0" type="HideDoor" x="32" y="64" width="64" height="192">
    <properties>
     <property name="doortype" value="hide"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
