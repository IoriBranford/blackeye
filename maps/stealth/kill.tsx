<?xml version="1.0" encoding="UTF-8"?>
<tileset name="kill" tilewidth="128" tileheight="256" tilecount="2" columns="2">
 <image source="kill.png" width="256" height="256"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="125"/>
   <frame tileid="1" duration="125"/>
  </animation>
 </tile>
</tileset>
