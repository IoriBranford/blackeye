# Levity #

A platform for 2D tile map game development using the LÖVE engine and the Tiled editor.

Currently used for prototyping a 2D stealth game.

![screenshot04.png](https://bitbucket.org/repo/pGM7or/images/2883059318-screenshot04.png)

![screenshot0003.png](https://bitbucket.org/repo/pGM7or/images/134366031-screenshot0003.png)

![screenshot05.png](https://bitbucket.org/repo/pGM7or/images/3839128482-screenshot05.png)

### Try It ###

Find the demo app in [Downloads](https://bitbucket.org/IoriBranford/levity/downloads).

Download one of the following:

* .love file
	* Install [LÖVE](https://love2d.org) and run the .love file with it
* Self-extracting archive
	* Extract and run

Game controls are onscreen.

### Developer Setup ###

* Dependencies
	* [Tiled](https://thorbjorn.itch.io/tiled) map editor
	* [LÖVE](https://love2d.org) game engine
	* [git](https://git-scm.com/) version control
* Setup
	1. Install above dependencies.
	1. Open Command Prompt or Terminal.
	1. Get code including submodules: `git clone --recursive https://bitbucket.org/IoriBranford/levity.git levity`
* Run
	1. `love <path-to-levity>`

### Contact ###

IoriBranford - ioribranford a gmail d com