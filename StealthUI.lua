local levity = require "levity"
require "class"

local StealthUI = class(function(self, id)
	self.layer = levity.map.layers[id]
	self.layer.visible = false
end)

--function StealthUI:keypressed_escape()
--	love.event.quit()
--end

function StealthUI:keypressed_escape()
	levity:setNextMap("maps/stealth_proto2.lua")
end

--function StealthUI:keypressed_f12()
--	local screenshotdata = love.graphics.newScreenshot()
--	for i = 1, 99, 1 do
--		local filename = string.format("screenshot%02d.png", i)
--		if not love.filesystem.exists(filename) then
--			screenshotdata:encode("png", filename)
--			return
--		end
--	end
--	print("Screenshot folder is full")
--end

function StealthUI:endMove(dt)
	self.layer.visible = true
	self.layer.offsetx = levity.camera.x
	self.layer.offsety = levity.camera.y
	local player = levity.map.objects[tonumber(levity.map.properties.playerid)]
	for i, object in ipairs(self.layer.objects) do
		if object.name == "moveleft" or object.name == "moveright" then
			object.visible = (player ~= nil)
		elseif object.name == "reset" then
			object.visible = (player == nil)
		end
	end
end

function StealthUI:beginDraw()
	love.graphics.setShader()
	love.graphics.setStencilTest()
end

return StealthUI
